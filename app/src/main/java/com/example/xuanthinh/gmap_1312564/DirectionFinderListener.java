package com.example.xuanthinh.gmap_1312564;

import java.util.List;

/**
 * Created by XuanThinh on 14/06/2016.
 */
public interface DirectionFinderListener {
    void onDirectionFinderStart();
    void onDirectionFinderSuccess(List<Route> route);
}
