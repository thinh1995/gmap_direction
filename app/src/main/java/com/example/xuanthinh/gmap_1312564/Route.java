package com.example.xuanthinh.gmap_1312564;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by XuanThinh on 14/06/2016.
 */
public class Route {
    public Distance distance;
    public Duration duration;
    public String endAddress;
    public LatLng endLocation;
    public String startAddress;
    public LatLng startLocation;

    public List<LatLng> points;
}
