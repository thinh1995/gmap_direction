package com.example.xuanthinh.gmap_1312564;

/**
 * Created by XuanThinh on 14/06/2016.
 */
public class Duration {
    public String text;
    public int value;

    public Duration(String text, int value) {
        this.text = text;
        this.value = value;
    }
}
